
let L: any;
if (process.client) {
  L = require('leaflet');
}

export const MakingOf = L.icon({
  iconUrl: 'marker-making-of.svg',
  iconSize:     [69, 75],
  iconAnchor:   [34, 56],
});

export const Poi = L.icon({
  iconUrl: 'marker-poi.svg',
  iconSize:     [69, 75],
  iconAnchor:   [34, 56],
});

export const Live = L.icon({
  iconUrl: 'marker-live.svg',
  iconSize:     [69, 75],
  iconAnchor:   [34, 56],
});