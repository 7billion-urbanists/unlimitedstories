import Vue from 'vue';

export interface GeneralState {
  height: number|null;
  width: number|null;
}

const getDefaultState = () => ({
  height: null,
  width: null,

});

export const state: () => GeneralState = getDefaultState;

export const mutations = {
  setSize(state: GeneralState, {width, height}: {width: number; height: number;}) {
    state.width = width;
    state.height = height;
  }
}

export const actions = {
  setSize({commit}: any, {width, height}: {width: number; height: number;}) {
    commit('setSize', {width, height});
  }
}

export const getters = {
  smallWidth (state: GeneralState) {
    if ((state.width || 0) < 1024) {
      return true;
    }
    return false;
  }
}
