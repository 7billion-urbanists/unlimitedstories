import Vue from 'vue';

export interface MapState {
  filters: any; 
}

export interface MapFilter {
  name: string;
  value: boolean;
}

const getDefaultState = () => ({
  filters: {
    types: {
      'point-of-interest': {
        name: "Point d'intérêt",
        value: true
      },
      'live': {
        name: "Live",
        value: true
      },
      'making-of': {
        name: "Making-of",
        value: true
      }
    },
    dimensions: {
      nature: {
        name: "Nature",
        value: true
      },
      mobility: {
        name: "Mobilité",
        value: true
      },
      life: {
        name: "Vie du village",
        value: true
      },
      identity: {
        name: "Identité",
        value: true
      },
    },
  }
});

export const state: () => MapState = getDefaultState;

export const mutations = {
  switchFilter(state: MapState, {category, name}: {category: string; name: string;}){
    state.filters[category][name].value = !state.filters[category][name].value;
  }
}

export const actions = {
  switchFilter({commit}: any, {category, name}: {category: string; name: string;}) {
    commit('switchFilter', {category, name});
  }
}
