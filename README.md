# -- À PROPOS DU PROTOTYPE UNLIMITED STORIES® --

Le logiciel libre Unlimited Stories® est en phase de développement (version prototype). Le code sous licence GNU AGPL 3.0  donne accès à un prototype fonctionnel dit “Carte des Récits” destiné à susciter et partager les différents récits liés à un territoire dans une logique d’urbanisme ouvert. Les auteurs des récits sont l’ensemble des personnes présentent sur le territoire considéré ou ayant un intérêt pour ce territoire (citoyens, professionnels, autorités). 

Les versions 1.0 et suivantes du logiciel sont en cours de planification et de développement avec le soutien du réseau https://7billion-urbanists.org/ , des fondations Free-IT Foundation https://freeit.world et Open Urbanism Foundation https://www.openurbanism.world,  ainsi que le village d’Anières en Suisse « https://anieres.ch ». Les versions 1.X et suivantes seront accompagnées de documentations. 

Auteurs LOGICIEL UNLIMITED STORIES® prototype : Host lab, Nicolas Ancel, Carole Dureau, Alain Renk, la commune d’Anieres Claudine Hentsch et les citoyens contributeurs; Catherine Girod, François Dunant, Thomas Gil, Bertrand Carlier. Remerciement à tous les auteurs des récits. 

Site web officiel : http://unlimitedcities.world

Developer: Nicolas Ancel <ancelnicolas91@gmail.com>

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
