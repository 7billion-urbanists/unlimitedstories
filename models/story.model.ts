import { Article } from './article.model';
import { Location } from '@/models/location.model';

export interface Story {
  id: string;
  location: Location,
  type: StoryType;
  dimension: DimensionType;
  title: string;
  subtitle?: string;
  description: string;
  picture: string;
  links: {
    video?: string;
    audio?: string;
    document?: string;
    article?: boolean;
  }
  authors?: string;
  date?: Date;
  publicationDate: Date;
  article?: Article;
}


export type StoryType = 'making-of'|'point-of-interest'|'live';
export type DimensionType = 'nature'|'mobility'|'life'|'identity';