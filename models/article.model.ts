export interface Article {
  images?: {
    src: string,
    legend?: string,
  }[];
  text?: string;
  videos?: string[];
  audios?: string[];
  documents?: string[];
};
